# Contributor: Steven Guikal <void@fluix.one>
# Maintainer: Philipp Glaum <p@pglaum.de>
pkgname=py3-flask-migrate
_pkgname=Flask-Migrate
pkgver=4.0.4
pkgrel=2
pkgdesc="SQLAlchemy database migrations for Flask applications using Alembic"
url="https://pypi.org/project/Flask-Migrate/"
arch="noarch"
license="MIT"
depends="
	py3-alembic
	py3-flask
	py3-flask-sqlalchemy
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$_pkgname-$pkgver-2.tar.gz::https://github.com/miguelgrinberg/Flask-Migrate/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	PYTHONPATH="$PWD/src" .testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
66d7a96aafe81e94b59f3e510036dda82047c53a9bf67654ee5fe77ee30361f516a36000648f58de2743e1fa288302be76a82601a1eeae65f0acb8fd486fa9c1  Flask-Migrate-4.0.4-2.tar.gz
"
